const https = require('https');

const getTheJson = (prefix, suffix) => {
    return new Promise(function (resolve, reject) {
        let counter = 0;
        https.get(`https://api.pwnedpasswords.com/range/${prefix}`, (res) => {
            console.log('statusCode:', res.statusCode);
            let passwordBody;

            res.on('data', data => {
                passwordBody += data;
            });

            res.on('end', () => {
                let passwordArray = passwordBody.split('\r\n');
                passwordArray.forEach(element => {
                    if (element.includes(suffix)) {
                        counter = (element.split(':').pop());
                    }
                });
                resolve(counter);
            });

        }).on('error', (e) => {
            console.error(e);
            reject(e);
        });
    });
    console.log(`end counter ${counter}`);
    return counter;
}

module.exports = {
    getTheJson
}
