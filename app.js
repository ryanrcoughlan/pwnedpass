const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const https = require('https');
const getJson = require('./get');
const sha1 = require('sha1');

const app = express();

app.use(express.static('public'));

app.use(bodyParser.urlencoded({ extended: false}));
app.use(cookieParser());

app.set('view engine', 'pug');

app.get('/',(req, res)=>{
    res.render('index');
});

app.post('/sendPassword',(req, res)=>{
    let hashedPassword = sha1(req.body.passwordTing);
    let counter = getJson.getTheJson(hashedPassword.substring(0,5),hashedPassword.substring(5,hashedPassword.size).toUpperCase())
    .then((counter) => {
        console.log(`counter: ${counter}`);
        res.render('index', {counter});
    });
});

app.use((req, res, next) => {
    const err = new Error('not found');
    err.status = 404;
    next(err);
})

app.use((err, req, res, next) => {
    res.locals.error = err;
    res.status(err.status);
    res.render('error');
})

app.listen(8081, () => {
    console.log('The application is running on localhost:8081')
});


