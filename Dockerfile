FROM node:8
WORKDIR /app
COPY package.json /app
COPY package-lock.json /app
RUN npm install -g nodemon
RUN npm install
COPY . /app
CMD node app.js
EXPOSE 80